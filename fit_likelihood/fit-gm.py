from datetime import datetime
from scipy.io import loadmat, savemat
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
import numpy as np
import os
import torch
import torch.nn.functional as F
import torch.onnx
import torch.optim as optim
from pathlib2 import Path
import logging

import fit_likelihood.general as gn
import math
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

log     = logging.getLogger(__name__)


wantToUseCuda   = True
useCuda         = torch.cuda.is_available() and wantToUseCuda
device          = 'cuda' if useCuda else 'cpu'
dtype           = torch.float32


# ---------------------- TYPE OF FLOW ALG ----------------------
OF_LKPYR            = "lk-pyr"
OF_FARNEBACK        = "fb"
OF_TYPE             = OF_LKPYR
training_fps        = "480"    
dimensions          = "360x640"

traj_straight   = "straight"
traj_curved     = "curved"
traj_forest     = "forest"
traj_city       = "city"
traj_cityforest = "city_forest"
traj_type       = traj_city

# --------------------- Load directories ---------------------
DIR_DATASET_ROOT= gn.getDatasetRoot()
NAME_EXP        = "empericaldistributions/" + OF_TYPE + "/dim" + dimensions + "_fps" + training_fps + "_traj_" + traj_type
DIR_EXP_ROOT    = os.path.abspath(DIR_DATASET_ROOT + "/" + NAME_EXP)
DIR_EXP_DATA    = os.path.abspath(DIR_EXP_ROOT + "/data")
DIR_EXP_LUT     = os.path.abspath(DIR_EXP_ROOT + "/lut")



# -------------------------- Get data -------------------------
mat_files       = [f for f in os.listdir(DIR_EXP_DATA) if os.path.isfile(os.path.join(DIR_EXP_DATA, f)) and f.startswith("framedata_")]
mat_files.sort()
log.info("%d frame data files found in %s", len(mat_files), DIR_EXP_DATA)
assert len(mat_files)>0, "Expected there to be frame data"

from sklearn.model_selection import train_test_split
test_size       = 0.2
train_files, test_files = train_test_split(mat_files, test_size=test_size, shuffle=False)




batch_size_train    = 64
batch_size_test     = 100

try:
    train_data      = gn.TextureFlowErrorDataset(DIR_EXP_DATA, train_files, use_cache=True)
except MemoryError:
    train_data      = gn.TextureFlowErrorDataset(DIR_EXP_DATA, train_files, use_cache=False)

train_loader    = DataLoader(train_data, batch_size=batch_size_train, shuffle=True, collate_fn=gn.my_collate)

test_data       = gn.TextureFlowErrorDataset(DIR_EXP_DATA, test_files)
test_loader     = DataLoader(test_data, batch_size=batch_size_test, shuffle=True, collate_fn=gn.my_collate)

nz  = 0

ndisp = 50
nmod  = math.floor(len(train_loader) / float(ndisp))
for batch_idx, (t, z) in enumerate(train_loader):
    nz += z.numel()
    if (batch_idx % nmod) == 0:
        log.info("Loading %g %% of training loader", 100 * batch_idx / float(len(train_loader)))
log.info("%d data points in training set", nz)

datapoints      = 5e10
nepochs         = math.ceil(float(datapoints)/nz)
maxepoch        = 100
if nepochs > maxepoch:
    log.info("%d epochs to run. Capping at %d epochs", nepochs, maxepoch)
    nepochs         = maxepoch

log.info("Setting up with %d epochs, a training batch size of %d and a test batch size of %d", nepochs, batch_size_train, batch_size_test)


train_losses    = []
train_counter   = []
test_losses     = []
test_counter    = [i*len(train_loader.dataset) for i in range(1,nepochs + 1)]


class SIGM(torch.nn.Module):
    def __init__(self, nLUTdof=3, nGM=10, dtype=torch.float64, device="cpu"):
        super().__init__()
        nu                  = 25
        self.maxTex         = 10000
        self.minTex         = 0.1


        self.dtype          = dtype
        self.device         = device
        self.nGM            = nGM
        self.nLUTdof        = nLUTdof
        self.nu             = nu

        log.info("Initialise model with nGM = %d, nLUTdof = %d, nu = %d", self.nGM, self.nLUTdof, self.nu )
        self.initIndices()
        self.setInitialState()
        

    def initIndices(self) -> None:
        self.induce_tex     = torch.logspace(
            torch.tensor(self.minTex).log10(), 
            torch.tensor(self.maxTex).log10(), 
            self.nu).reshape((self.nu,1)).to(dtype=self.dtype, device=self.device)
        idx                 = torch.arange(0, self.nGM, dtype=torch.long, device=self.device)
        self.idxlw          = idx
        self.idxnu          = self.nGM + idx
        self.idxxi          = self.nGM*2 + idx

    def setInitialState(self) -> None:
        theta0              = torch.zeros((self.nLUTdof, 1), dtype=self.dtype, device=self.device)
        self.u              = torch.nn.Parameter(theta0.repeat((self.nu,1)))
        self.fc1            = torch.nn.Linear(self.nLUTdof, 3*self.nGM, dtype=self.dtype, device=self.device)

    def forward(self, t:  torch.Tensor) ->  torch.Tensor:
        # theta - [nd x ntheta]
        theta               = gn.lut(t, self.induce_tex, self.u).T
        # phi - [nd x nphi]
        phi                 = self.fc1(theta)
        phi[:, self.idxlw]  = F.log_softmax(phi[:, self.idxlw], dim=1)
        phi[:, self.idxxi]  = phi[:, self.idxxi].exp()
        # logwtilde     = phi[:, self.idxlw]
        # nu            = phi[:, self.idxnu]
        # xi            = phi[:, self.idxxi]
        return phi

    def likelihood(self, phi: torch.Tensor, z:  torch.Tensor) -> torch.Tensor:
        lw      = phi[:, self.idxlw] 
        nu      = phi[:, self.idxnu]
        xi      = phi[:, self.idxxi]
        ll      = lw + gn.log_normpdfSi(z, nu, xi)
        l       = ll.logsumexp(dim=1)
        return l

    def cost(self, phi:  torch.Tensor, z:  torch.Tensor) ->  torch.Tensor:
        l       = -self.likelihood(phi, z).mean()
        return l

    def saveMat(self, filepath : str, savedict : dict =None) -> None:
        if savedict is None:
            savedict    = dict()

        for param_tensor in self.state_dict():
            matname             = param_tensor.replace(".", "_")
            savedict[matname]   =  self.state_dict()[param_tensor].detach().cpu().numpy()
        savedict["date"]    = str(datetime.now())
        savedict["dtype"]   = str(self.dtype)
        savedict["maxTex"]  = self.maxTex
        savedict["minTex"]  = self.minTex
        savedict["nGM"]     = self.nGM
        savedict["nLUTdof"] = self.nLUTdof
        savedict["nu"]      = self.nu


        log.info("Saving model to %s", filepath)
        
        savemat(filepath, savedict)

    def loadMat(self, filepath : str) -> None:

        log.info("Loading model from %s", filepath)

        data            = loadmat(filepath)
        self.maxTex     = data["maxTex"] 
        self.minTex     = data["minTex"] 
        self.nGM        = data["nGM"]    
        self.nLUTdof    = data["nLUTdof"]
        self.nu         = data["nu"]
        for param_tensor in self.state_dict():
            matname             = param_tensor.replace(".", "_")
            self.state_dict()[param_tensor] = torch.from_numpy(data[matname]).to(dtype=self.dtype, device=self.device)

model           = SIGM(nGM=15, dtype=dtype, device=device)
# filename    = "sigm_epoch1_nGM30_nu25.mat"
# filepath    = os.path.join(DIR_EXP_LUT, filename)
# model.loadMat(filepath)

learning_rate   = 0.001
optimizer       = optim.AdamW(model.parameters(),
                            lr=learning_rate)


nz      = 0
def train(epoch):

    ndisp = 10
    nmod  = math.floor(len(train_loader) / float(ndisp))

    for batch_idx, (t, z) in enumerate(train_loader):
        optimizer.zero_grad()
        t,z     = t.to(dtype=dtype, device=device),z.to(dtype=dtype, device=device)
        phi     = model(t)
        loss    = model.cost(phi, z)
        loss.backward()
        optimizer.step()
        if epoch == 1:
            global nz
            nz += t.numel()

        if batch_idx % nmod == 0:
            log.info("Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}".format(
                epoch,
                batch_idx * train_loader.batch_size,
                len(train_loader.dataset),
                100. * batch_idx / len(train_loader),
                loss.item()))

        train_losses.append(loss.item())
        train_counter.append(
                (batch_idx*batch_size_train) + ((epoch-1)*len(train_loader.dataset)))

def test():

    test_loss = 0
    correct = 0
    with torch.no_grad():
        nbatches    = 0
        for t, z in test_loader:
            t,z     = t.to(dtype=dtype, device=device),z.to(dtype=dtype, device=device)
            phi     = model(t)
            loss    = model.cost(phi, z)
            test_loss   += loss
            nbatches    += 1

    test_loss /= nbatches
    test_losses.append(test_loss.item())
    log.info('Test set: Avg. loss: {:.4f}\n'.format(
        test_loss))

for epoch in range(1, 1+nepochs):
    train(epoch)
    test()


fig = plt.figure(1)
plt.plot(test_counter, test_losses, 'r-', label='Test Loss')
plt.ylabel("Negative log-likelihoood loss")
plt.xlabel("No. of images seen")
plt.legend()
plt.draw()
plt.pause(0.001)

fig = plt.figure(2)
plt.plot(train_counter, train_losses, label="Train Loss")
plt.plot(test_counter, test_losses, 'r-', label='Test Loss')
plt.ylabel("Negative log-likelihoood loss")
plt.xlabel("No. of images seen")
plt.legend()
plt.draw()
plt.pause(0.001)


dtype   = model.dtype
ne      = 4
t       = torch.logspace(torch.tensor(0), torch.tensor(7500).log10(), ne).to(dtype=dtype, device=device).reshape((-1,1))
z       = 3*torch.linspace(torch.tensor(-1), torch.tensor(1),10000).to(dtype=dtype, device=device).reshape((-1,1))
fig = plt.figure(3)

for i in range(ne):
    te      = t[i].reshape((-1,1))
    phi     = model(te)
    l       = model.likelihood(phi, z)
    plt.subplot(1,ne, i+1)
    plt.plot(z.cpu(), l.detach().cpu().exp())
    plt.ylabel("LUT p(z)")
    plt.xlabel("z")
    plt.title("Texture - %g" % te.item() )
    plt.draw()

plt.pause(0.001)

# To save
savedict    = dict()
savedict['data_dir']        = DIR_EXP_DATA
# savedict['date']            = str(datetime.now())
# savedict['induce_tex']      = model.induce_tex.numpy()
savedict['nz']              = nz
savedict['test_counter']    = test_counter
savedict['test_losses']     = test_losses
savedict['train_counter']   = train_counter
savedict['optimizer']       = str(optimizer)
savedict['train_losses']    = train_losses
# savedict['u']               = ustar.numpy()
filename    = "sigm_epoch%d_nGM%d_nLUT%d_nu%d.mat" % (nepochs, model.nGM, model.nLUTdof, model.nu)
Path(DIR_EXP_LUT).mkdir(parents=True, exist_ok=True)
filepath    = os.path.join(DIR_EXP_LUT, filename)
model.saveMat(filepath, savedict)
# savemat(filepath, savedict)

print("LUT mapping: ")
print(model.u.detach().cpu().numpy())

print("\nAffine weight matrix: ")
print(model.fc1.weight.detach().cpu().numpy())

print("Affine bias: ")
print(model.fc1.bias.detach().cpu().numpy())

t       = torch.tensor(1).to(dtype=dtype, device=device).reshape((-1,1))
phi     = model(t)
print("Predicted parameters when t = %g: " % (t.item()))
print(phi.detach().cpu().numpy())

input("Press [enter] to continue.")