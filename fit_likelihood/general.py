from datetime import datetime
from scipy.io import loadmat, savemat
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
import numpy as np
import os
import torch
import torch.nn.functional as F
import torch.onnx
import torch.optim as optim
from pathlib2 import Path
import logging

import math

log     = logging.getLogger(__name__)


def reshape_fortran(x: torch.Tensor, shape) ->torch.Tensor:
    if len(x.shape) > 0:
        x = x.permute(*reversed(range(len(x.shape))))
    return x.reshape(*reversed(shape)).permute(*reversed(range(len(shape))))

def lut(t: torch.Tensor, tex_induce: torch.Tensor, u: torch.Tensor) -> torch.Tensor:
    nlut    = tex_induce.numel()
    nz      = t.numel()
    ntheta  = u.numel()/nlut
    assert ntheta == round(ntheta), "Expected ntheta to be a whole number"
    nlut    = int(nlut)
    ntheta  = int(ntheta)
    nz      = int(nz)
    dtype   = u.dtype
    device  = u.device

    dolog           = True
    if dolog:
        t           = t.log()
        tex_induce  = tex_induce.log()
    

    # Fix for FORTRAN style interpretation in MATLAB
    theta_induce    = u.reshape((nlut, ntheta)).T

    tex_lb          = tex_induce[:-1].T
    tex_ub          = tex_induce[1::].T
    theta_lb        = theta_induce[:, :-1]
    theta_ub        = theta_induce[:, 1::]
    
    idxInterp       = (tex_induce[0] < t) & (t < tex_induce[-1])
    niterp          = idxInterp.sum()
    tinterp         = t[idxInterp].reshape((-1,1))
    idx             = torch.arange(0, nlut-1, dtype=torch.long)
    Idx             = idx * torch.ones((niterp, 1), dtype=torch.long)

    lb              = tex_lb < tinterp
    ub              = lb & (tinterp <= tex_ub)
    interb          = lb & ub
    idx_            = torch.arange(0, niterp*(nlut-1))
    idxa            = np.unravel_index(idx_[interb.flatten()], (niterp, nlut-1), order="C")
    interbNidx      = Idx[idxa[0], idxa[1]]
    delta           = (tinterp.flatten() - tex_lb[0,interbNidx])/(tex_ub[0,interbNidx] - tex_lb[0,interbNidx])

    assert (delta>1).any() == False, "Expected delta to be no greater than 1"
    assert (delta<0).any() == False, "Expected delta to be no less than 1"

    # Find knots to extrapolate from
    idxLowExtrap    = t <= tex_induce[0]
    idxUpperExtrap  = tex_induce[-1] < t
    
    theta           = torch.ones((ntheta, nz), dtype=dtype, device=device)
    if idxLowExtrap.any():
        theta[:,idxLowExtrap.flatten()]     = theta_induce[:,[0]].repeat(1, idxLowExtrap.sum())
    if idxUpperExtrap.any():
        theta[:,idxUpperExtrap.flatten()]   = theta_induce[:,[-1]].repeat(1, idxUpperExtrap.sum())
    if idxInterp.any():
        theta[:, idxInterp.flatten()]       = theta_lb[:, interbNidx]*(1-delta) + theta_ub[:, interbNidx]*(delta)
    
    
    return theta

# Distributions
def log_normpdfSi(x: torch.Tensor, nu: torch.Tensor, xi: torch.Tensor) -> torch.Tensor:
    z   = xi*x - nu
    eps = 1e-20
    l   = -0.5*torch.log(torch.tensor(2*torch.pi, dtype=z.dtype)) + torch.log(xi.abs() + eps) - 0.5*z*z
    return l

def log_laplacepdf(x: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
    """Calculates the log of the scaled laplace distribution 

    p(x) = exp(-abs(x)/b)/2b
    Args:
        x (torch.Tensor): error
        beta (torch.Tensor):

    Returns:
        torch.Tensor: _description_
    """
    return -math.log(2) - b.log() - x.abs() / b

def log_cauchypdf(x: torch.Tensor, gamma: torch.Tensor) -> torch.Tensor:
    """Calculates the log of the scaled cauchy distribution

    Args:
        x (torch.Tensor): error
        gamma (torch.Tensor): gamma 

    Returns:
        torch.Tensor: _description_
    """

    return gamma.log() - math.log(math.pi) - (gamma*gamma + x*x).log()


# Datasets
class TextureFlowErrorDataset(Dataset):
    def __init__(self, mat_dir, files, dtype=torch.float32, device="cpu", use_cache=False):
        self.data_dir   = mat_dir
        self.files      = files
        self.dtype      = dtype
        self.device     = device
        self.use_cache  = use_cache

        if self.use_cache:
            ndisp = 50
            nmod  = math.floor(len(self.files) / float(ndisp))
            self.cache_t = []
            self.cache_z = []
            ndatapoints  = 0
            for i in range(len(self.files)):
                if (i % nmod) == 0:
                    log.info("Caching %g %% of data", 100 * i / float(len(self.files)))
                t, z = self.getItemFromFile(i)
                ndatapoints += t.numel()
                self.cache_t.append(t)
                self.cache_z.append(z)

            sizeof_t_bits   = torch.finfo(t.dtype).bits * ndatapoints
            sizeof_t        = sizeof_t_bits / 8
            sizeof_dataset  = sizeof_t_bits / 4

            log.info("Cached dataset is %e bytes with %d elements", sizeof_dataset, ndatapoints)



    def __len__(self):
        return len(self.files )

    def getItemFromFile(self, idx):
        mat_path    = os.path.join(self.data_dir, self.files[idx])
        data        = loadmat(mat_path)
        eig         = data['eigenvalues']
        err         = data['dpdfe_true'] - data['dpdfe_est']
        z           = torch.from_numpy(err).reshape((-1,1)).to(device=self.device, dtype=self.dtype)
        t           = torch.from_numpy(eig).reshape((-1,1)).to(device=self.device, dtype=self.dtype)
        return t, z

    def getItemFromCache(self, idx):
        return self.cache_t[idx], self.cache_z[idx]

    def __getitem__(self, idx):
        if self.use_cache:
            return self.getItemFromCache(idx)
        else:
            return self.getItemFromFile(idx)



def my_collate(batch):
    nb  = len(batch)
    nd  = 0
    dtype   = []
    device  = []
    for packet in batch:
        nd      += packet[0].numel()
        dtype   = packet[0].dtype
        device  = packet[0].device

    t       = torch.zeros((nd,1), dtype=dtype, device=device)    
    z       = torch.zeros_like(t)
    idxPacketStart    = 0
    for packet in batch:
        n               = packet[0].numel()
        idx             = idxPacketStart + torch.arange(0, n, dtype=int, device=device)
        t[idx]          = packet[0].reshape((n,1))
        z[idx]          = packet[1].reshape((n,1))

        idxPacketStart  += n
    return t, z


# Directories
from os.path import expanduser
def getDirDropbox()-> str:
    dir_home        = expanduser("~")
    
    dir_dropbox     = os.path.join(dir_home, "Dropbox")
    assert os.path.isdir(dir_dropbox), "Expected Dropbox directory to exist"
    return dir_dropbox

def getDatasetRoot()-> str:
    path_dropbox = getDirDropbox()
    path_dataset = path_dropbox + "/Uni/PhD/datasets"

    return path_dataset

def getExternalDrive() -> str:
    useExternalHarddrive = os.path.isdir("/media/es313/")
    if useExternalHarddrive:
        os.system("sudo mount /dev/sda3 /media/es313/DATA_linux")
        return "/media/es313/DATA_linux"
    else:
        return None