Fit likelihood model for optical flow

## References

Farnworth, T., Renton, C., Strydom, R., Wills, A., Perez, T., 2021. A heteroscedastic likelihood model for two-frame optical flow. IEEE Robotics and Automation Letters 6 (2), 1200–1207.
